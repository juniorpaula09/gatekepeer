package dtos

// TypeBotResponse is the struct for the response from TypeBot
type TypeBotResponse struct {
	SessionID string `json:"sessionId"`
	Typebot   struct {
		ID string `json:"id"`
	} `json:"typebot"`
	Messages []struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Content struct {
			RichText []struct {
				Type     string `json:"type"`
				Children []struct {
					Text string `json:"text"`
				} `json:"children"`
			} `json:"richText"`
		} `json:"content"`
	} `json:"messages"`
	Input struct {
		ID    string `json:"id"`
		Type  string `json:"type"`
		Items []struct {
			ID             string `json:"id"`
			OutgoingEdgeId string `json:"outgoingEdgeId"`
			Content        string `json:"content"`
		} `json:"items"`
	} `json:"input"`
}
