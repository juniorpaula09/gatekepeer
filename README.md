# Gatekeeper

***Gatekeeper*** é um porteiro que faz a ponte entre o `agent bot` do ***ChatWoot*** e o fluxo de chat do ***Typebot***

#### Executar
```bash
$ go build -o ./bin/bot ./cmd/main.go 
$ ./bin/bot
```