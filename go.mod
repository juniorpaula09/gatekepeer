module cw-agentbot

go 1.21.8

require (
	github.com/go-chi/chi v1.5.5
	github.com/mattn/go-sqlite3 v1.14.22
)

require github.com/joho/godotenv v1.5.1
