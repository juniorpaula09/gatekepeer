package repository

import (
	"context"
	"cw-agentbot/internal/models"
	"database/sql"
	"time"
)

type IntegrationInterface interface {
	Insert(models.Integration) error
	FindByAccountID(accountID int) (models.Integration, error)
}

type IntegrationSQLiteRepo struct {
	DB *sql.DB
}

func NewIntegrationSQLiteRepo(db *sql.DB) *IntegrationSQLiteRepo {
	return &IntegrationSQLiteRepo{
		DB: db,
	}
}

func (r *IntegrationSQLiteRepo) Insert(integration models.Integration) error {
	stmt, err := r.DB.Prepare(`
		INSERT INTO integration 
			(cw_agent_bot_id, cw_agent_bot_name, cw_agent_bot_access_token, cw_access_token, cw_account_id, tb_bot_id, tb_workspace_id, tb_workspace_name)
		 VALUES (?, ?, ?, ?, ?, ?, ?, ?)`)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err = stmt.ExecContext(
		ctx,
		integration.CWAgentBotID,
		integration.CWAgentBotName,
		integration.CWAgentBotAcessToken,
		integration.CWAccessToken,
		integration.CWAccountID,
		integration.TBBotID,
		integration.TBWorkspaceID,
		integration.TBWorkspaceName)
	if err != nil {
		return err
	}

	return nil
}

func (r *IntegrationSQLiteRepo) FindByAccountID(accountID int) (models.Integration, error) {
	var integration models.Integration

	stmt, err := r.DB.Prepare(`SELECT * FROM integration WHERE cw_account_id = ?`)
	if err != nil {
		return integration, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	err = stmt.QueryRowContext(ctx, accountID).Scan(
		&integration.ID,
		&integration.CWAgentBotID,
		&integration.CWAgentBotName,
		&integration.CWAgentBotAcessToken,
		&integration.CWAccessToken,
		&integration.CWAccountID,
		&integration.TBBotID,
		&integration.TBWorkspaceID,
		&integration.TBWorkspaceName,
		&integration.CreatedAt)
	if err != nil {
		return integration, err
	}

	return integration, nil
}
