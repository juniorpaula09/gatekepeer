package main

import (
	"cw-agentbot/internal/config"
	"cw-agentbot/internal/routes"
	"fmt"
	"log"
	"net/http"

	"github.com/joho/godotenv"
	_ "github.com/mattn/go-sqlite3"
)

var port = "6969"

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	cfg, err := config.NewOpenconnection()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Server is running on port: %s\n", port)
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: routes.Setup(cfg.DB),
	}

	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
