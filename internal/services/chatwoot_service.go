package services

import (
	"bytes"
	"cw-agentbot/internal/dtos"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type ChatwootService interface {
	SendMessage(message string) (map[string]interface{}, error)
	CreateAgentBot(agentBot dtos.AgentBotDTO) (dtos.AgentBotDTO, error)
	GetAccountDetails(accountID int) (dtos.CWAccountDTO, error)
}

type ChatwootServiceImpl struct {
	URL            string
	AccountID      int
	ConversationID int
	AccessToken    string
}

func NewChatwootService(account, conversation int, accessToken, url string) *ChatwootServiceImpl {
	return &ChatwootServiceImpl{
		URL:            url,
		AccountID:      account,
		ConversationID: conversation,
		AccessToken:    accessToken,
	}
}

// chatWootRequest is a struct to send a message to ChatWoot
type chatWootRequest struct {
	Content string `json:"content"`
}

// SendMessage sends a message to a conversation in ChatWoot
// It receives the message to be sent and returns the response from the API
func (cw *ChatwootServiceImpl) SendMessage(message string) (map[string]interface{}, error) {
	data := chatWootRequest{
		Content: message,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/api/v1/accounts/%d/conversations/%d/messages", cw.URL, cw.AccountID, cw.ConversationID)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer((jsonData)))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("api_access_token", cw.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var response map[string]interface{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// CreateAgentBot creates an agent bot in ChatWoot
// It receives the agent bot data and the token for the API
func (cw *ChatwootServiceImpl) CreateAgentBot(agentBot dtos.AgentBotDTO) (dtos.AgentBotDTO, error) {
	jsonPayload, err := json.Marshal(agentBot)
	if err != nil {
		return dtos.AgentBotDTO{}, err
	}

	url := fmt.Sprintf("%s/api/v1/accounts/%d/agent_bots", cw.URL, cw.AccountID)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer((jsonPayload)))
	if err != nil {
		return dtos.AgentBotDTO{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("api_access_token", cw.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return dtos.AgentBotDTO{}, err
	}
	defer resp.Body.Close()

	result, err := io.ReadAll(resp.Body)
	if err != nil {
		return dtos.AgentBotDTO{}, err
	}

	var response dtos.AgentBotDTO
	err = json.Unmarshal(result, &response)
	if err != nil {
		return dtos.AgentBotDTO{}, err
	}

	return response, nil
}

// GetAccountDetails gets the details of an account in ChatWoot
// It receives the account ID and returns the details of the account
func (cw *ChatwootServiceImpl) GetAccountDetails(accountID int) (dtos.CWAccountDTO, error) {
	url := fmt.Sprintf("%s/api/v1/accounts/%d", cw.URL, accountID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return dtos.CWAccountDTO{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("api_access_token", cw.AccessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return dtos.CWAccountDTO{}, err
	}
	defer resp.Body.Close()

	result, err := io.ReadAll(resp.Body)
	if err != nil {
		return dtos.CWAccountDTO{}, err
	}

	var response dtos.CWAccountDTO
	err = json.Unmarshal(result, &response)
	if err != nil {
		return dtos.CWAccountDTO{}, err
	}

	return response, nil
}
