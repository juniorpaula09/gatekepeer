package models

import "time"

type Integration struct {
	ID                   int       `json:"id"`
	CWAgentBotID         int       `json:"cw_agent_bot_id"`
	CWAgentBotName       string    `json:"cw_agent_bot_name"`
	CWAgentBotAcessToken string    `json:"cw_agent_bot_access_token"`
	CWAccessToken        string    `json:"cw_access_token"`
	CWAccountID          int       `json:"cw_account_id"`
	TBBotID              string    `json:"tb_bot_id"`
	TBWorkspaceID        string    `json:"tb_workspace_id"`
	TBWorkspaceName      string    `json:"tb_workspace_name"`
	CreatedAt            time.Time `json:"created_at"`
}
