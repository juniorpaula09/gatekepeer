package dtos

// IncomingData is the struct for the incoming data from ChatWoot
// It contains the message type, content, conversation, sender and account
// The struct is used to decode the incoming data from ChatWoot webhook
type IncomingDataDTO struct {
	MessageType  string `json:"message_type"`
	Content      string `json:"content"`
	Conversation struct {
		ID int `json:"id"`
	}
	Sender struct {
		ID int `json:"id"`
	}
	Account struct {
		ID int `json:"id"`
	}
}

// AgentBot is the struct for the agent bot in ChatWoot
type AgentBotDTO struct {
	ID          int    `json:"id,omitempty"`
	AccountID   int    `json:"account_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	OutgoingURL string `json:"outgoing_url"`
	AcessToken  string `json:"access_token"`
}

type CWAccountDTO struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	SupportEmail string `json:"support_email"`
	Status       string `json:"status"`
}
