package config

import (
	"database/sql"
	"log"
)

type Config struct {
	DB *sql.DB
}

func NewOpenconnection() (*Config, error) {
	db, err := openDB()
	if err != nil {
		log.Println("Error opening database: ", err)
		return nil, err
	}

	return &Config{
		DB: db,
	}, nil
}

func openDB() (*sql.DB, error) {
	db, err := sql.Open("sqlite3", "./database.db")
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS integration (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		cw_agent_bot_id INTEGER,
		cw_agent_bot_name TEXT,
		cw_agent_bot_access_token TEXT,
		cw_access_token TEXT,
		cw_account_id INTEGER,
		tb_bot_id TEXT,
		tb_workspace_id TEXT,
		tb_workspace_name TEXT,
		created_at DATETIME DEFAULT CURRENT_TIMESTAMP
	);`)
	if err != nil {
		log.Println("Error creating table: ", err)
		return nil, err
	}

	return db, nil
}
