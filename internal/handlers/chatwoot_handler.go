package handlers

import (
	"cw-agentbot/internal/dtos"
	"cw-agentbot/internal/models"
	"cw-agentbot/internal/repository"
	"cw-agentbot/internal/services"
	"cw-agentbot/internal/utils"
	"log"
	"net/http"
	"os"
)

type ChatwootHandler struct {
	IntegrationDB repository.IntegrationInterface
}

func NewChatwootHandler(integrationDB repository.IntegrationInterface) *ChatwootHandler {
	return &ChatwootHandler{
		IntegrationDB: integrationDB,
	}
}

// jsonResponse is the struct for the JSON response from the API
type jsonResponse struct {
	Error   bool        `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

var agentBotInput struct {
	CWAccountID int    `json:"cw_account_id"`
	CWToken     string `json:"cw_token"`
	Name        string `json:"name"`
	Description string `json:"description"`
	OutgoingURL string `json:"outgoing_url"`
}

// CreateAgentBotHandler is the handler for creating an agent bot in ChatWoot
// It receives the account ID, name, description and outgoing URL of the agent bot
// It then sends the data to ChatWoot to create the agent bot
func (h *ChatwootHandler) CreateAgentBotHandler(w http.ResponseWriter, r *http.Request) {
	var payload jsonResponse

	err := utils.ReadJSON(w, r, &agentBotInput)
	if err != nil {
		payload = jsonResponse{
			Error:   true,
			Message: "Invalid request payload",
		}
		_ = utils.WriteJSON(w, http.StatusBadRequest, payload)
		return
	}

	// verify if the account already has an agent bot
	_, err = h.IntegrationDB.FindByAccountID(agentBotInput.CWAccountID)
	if err == nil {
		payload = jsonResponse{
			Error:   true,
			Message: "Account already has an agent bot",
		}
		_ = utils.WriteJSON(w, http.StatusBadRequest, payload)
		return
	}

	ab := dtos.AgentBotDTO{
		AccountID:   agentBotInput.CWAccountID,
		Name:        agentBotInput.Name,
		Description: agentBotInput.Description,
		OutgoingURL: agentBotInput.OutgoingURL,
	}

	chatwootService := services.NewChatwootService(ab.AccountID, 0, agentBotInput.CWToken, os.Getenv("CW_URL"))
	typebotService := services.NewTypeBotService("", os.Getenv("BOT_TOKEN"), os.Getenv("BOT_URL"), os.Getenv("BOT_WORKSPACE_URL"))

	agentBot, err := chatwootService.CreateAgentBot(ab)
	if err != nil {
		payload = jsonResponse{
			Error:   true,
			Message: "Error creating agent bot",
		}
		_ = utils.WriteJSON(w, http.StatusInternalServerError, payload)
		return
	}

	cwAccount, err := chatwootService.GetAccountDetails(ab.AccountID)
	if err != nil {
		payload = jsonResponse{
			Error:   true,
			Message: "Error getting account details",
		}
		_ = utils.WriteJSON(w, http.StatusInternalServerError, payload)
		return
	}

	// create typebot workspace
	workspace, err := typebotService.CreateWorkSpace(cwAccount.Name)
	if err != nil {
		payload = jsonResponse{
			Error:   true,
			Message: "Error creating agent bot",
		}
		_ = utils.WriteJSON(w, http.StatusInternalServerError, payload)
		return
	}

	// create typebot bot
	bot, err := typebotService.CreateTypeBot("BOT - Default", workspace.Workspace.ID)
	if err != nil {
		payload = jsonResponse{
			Error:   true,
			Message: "Error creating agent bot",
		}
		_ = utils.WriteJSON(w, http.StatusInternalServerError, payload)
		return
	}

	// save the bot id in the database
	integration := models.Integration{
		CWAgentBotID:         agentBot.ID,
		CWAgentBotName:       agentBot.Name,
		CWAgentBotAcessToken: agentBot.AcessToken,
		CWAccessToken:        agentBotInput.CWToken,
		CWAccountID:          agentBot.AccountID,
		TBBotID:              bot.TypeBot.ID,
		TBWorkspaceID:        workspace.Workspace.ID,
		TBWorkspaceName:      workspace.Workspace.Name,
	}
	err = h.IntegrationDB.Insert(integration)
	if err != nil {
		log.Println("Error saving agent bot", err)
		payload = jsonResponse{
			Error:   true,
			Message: "Error saving agent bot",
		}
		_ = utils.WriteJSON(w, http.StatusInternalServerError, payload)
		return
	}

	payload = jsonResponse{
		Error:   false,
		Message: "Agent bot created successfully",
	}
	utils.WriteJSON(w, http.StatusCreated, payload)
}
