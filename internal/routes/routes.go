package routes

import (
	"cw-agentbot/internal/handlers"
	"cw-agentbot/internal/repository"
	"database/sql"
	"net/http"

	"github.com/go-chi/chi"
)

func Setup(db *sql.DB) http.Handler {
	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello! I'm your favority bot :)"))
	})

	integrationRepo := repository.NewIntegrationSQLiteRepo(db)

	webhookHandler := handlers.NewWebhookHandler(integrationRepo)
	chatwootHandler := handlers.NewChatwootHandler(integrationRepo)

	r.Post("/webhook", webhookHandler.WebhookBotHandler)

	r.Post("/create-agent-bot", chatwootHandler.CreateAgentBotHandler)

	return r
}
