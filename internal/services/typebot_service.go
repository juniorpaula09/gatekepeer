package services

import (
	"bytes"
	"cw-agentbot/internal/dtos"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

type TypeBotService interface {
	SendMessage(message string) (dtos.TypeBotResponse, error)
	CreateWorkSpace(name string) (WorkspaceResponse, error)
	CreateTypeBot(name, workspaceID string) (BotResponse, error)
}

type TypeBotServiceImpl struct {
	BotURL          string
	BotWorkspaceURL string
	BotID           string
	BotToken        string
}

func NewTypeBotService(botID, botToken, botURL, botWorkspaceURL string) *TypeBotServiceImpl {
	return &TypeBotServiceImpl{
		BotURL:          botURL,
		BotWorkspaceURL: botWorkspaceURL,
		BotID:           botID,
		BotToken:        botToken,
	}
}

var sessionID string

var commands = map[rune]rune{
	'1': '1',
	'2': '2',
	'3': '3',
	'4': '4',
	'5': '5',
	'6': '6',
	'7': '7',
	'8': '8',
	'9': '9',
}

var options []struct {
	ID      string
	Content string
}

func (t *TypeBotServiceImpl) SendMessage(message string) (dtos.TypeBotResponse, error) {
	log.Println("Sending to TypeBot")
	if commands[rune(message[0])] != 0 {
		message = options[commands[rune(message[0])]-49].Content

		log.Println("Session ID exists, continuing chat...", sessionID)
		log.Println("Message: ", message)

		continueChatURL := fmt.Sprintf("%s/sessions/%s/continueChat", t.BotURL, sessionID)
		payload := fmt.Sprintf(`{"message": "%s"}`, message)
		log.Println("Payload: ", payload)
		req, err := http.NewRequest("POST", continueChatURL, bytes.NewBuffer([]byte(payload)))
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}

		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", t.BotToken))

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}
		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}

		var response dtos.TypeBotResponse

		err = json.Unmarshal(body, &response)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}
		log.Println("Response: ", response)
		return response, nil

	} else {
		startChatURL := fmt.Sprintf("%s/typebots/%s/preview/startChat", t.BotURL, t.BotID)

		req, err := http.NewRequest("POST", startChatURL, nil)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Accept", "application/json")
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", t.BotToken))

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}
		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}

		var response dtos.TypeBotResponse

		err = json.Unmarshal(body, &response)
		if err != nil {
			return dtos.TypeBotResponse{}, err
		}

		sessionID = response.SessionID

		for _, items := range response.Input.Items {
			options = append(options, struct {
				ID      string
				Content string
			}{ID: items.ID, Content: items.Content})
		}

		return response, nil
	}
}

// WorkspaceResponse is the response from TypeBot when creating a workspace
type WorkspaceResponse struct {
	Workspace struct {
		ID   string `json:"id"`
		Name string `json:"name"`
		Plan string `json:"plan"`
	}
}

// CreateWorkSpace creates a workspace in TypeBot
func (t *TypeBotServiceImpl) CreateWorkSpace(name string) (WorkspaceResponse, error) {
	url := fmt.Sprintf("%s/workspaces", t.BotWorkspaceURL)
	payload := fmt.Sprintf(`{"name": "%s"}`, name)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(payload)))
	if err != nil {
		return WorkspaceResponse{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", t.BotToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return WorkspaceResponse{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return WorkspaceResponse{}, err
	}

	var response WorkspaceResponse

	err = json.Unmarshal(body, &response)
	if err != nil {
		return WorkspaceResponse{}, err
	}

	return response, nil
}

// TypeBotResponse is the response from TypeBot when creating a bot
type BotResponse struct {
	TypeBot struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"typebot"`
}

// CreateTypeBot creates a bot in TypeBot
func (t *TypeBotServiceImpl) CreateTypeBot(name, workspaceID string) (BotResponse, error) {
	url := fmt.Sprintf("%s/typebots", t.BotWorkspaceURL)

	payload := fmt.Sprintf(`{"workspaceId": "%s", "typebot": {"name": "%s"}}`, workspaceID, name)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(payload)))
	if err != nil {
		return BotResponse{}, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", t.BotToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return BotResponse{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return BotResponse{}, err
	}

	var response BotResponse

	err = json.Unmarshal(body, &response)
	if err != nil {
		return BotResponse{}, err
	}

	return response, nil
}
