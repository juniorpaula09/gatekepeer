package handlers

import (
	"cw-agentbot/internal/dtos"
	"cw-agentbot/internal/repository"
	"cw-agentbot/internal/services"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type WebhookHanlder struct {
	IntegrationDB repository.IntegrationInterface
}

func NewWebhookHandler(integrationDB repository.IntegrationInterface) *WebhookHanlder {
	return &WebhookHanlder{
		IntegrationDB: integrationDB,
	}
}

// WebhookBotHandler is the handler for the incoming webhook from ChatWoot
// It receives the incoming data from ChatWoot and sends it to TypeBot
// It then sends the response from TypeBot back to ChatWoot as a reply
func (wh *WebhookHanlder) WebhookBotHandler(w http.ResponseWriter, r *http.Request) {
	var data dtos.IncomingDataDTO
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Get the account details
	account, err := wh.IntegrationDB.FindByAccountID(data.Account.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	chatwootService := services.NewChatwootService(data.Account.ID, data.Conversation.ID, account.CWAccessToken, os.Getenv("CW_URL"))
	typebotService := services.NewTypeBotService("BotID", os.Getenv("BOT_TOKEN"), os.Getenv("BOT_URL"), os.Getenv("BOT_WORKSPACE_URL"))

	log.Print("Received data from CW BOT\n")
	if data.MessageType == "incoming" {
		// Send to TypeBot

		typebotResponse, err := typebotService.SendMessage(data.Content)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// loop through the messages and send them to ChatWoot
		for _, m := range typebotResponse.Messages {
			// Send to ChatWoot
			_, err = chatwootService.SendMessage(m.Content.RichText[0].Children[0].Text)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			time.Sleep(1 * time.Second)
		}

		msgChoise := "Escolha uma opção abaixo:\n"
		_, err = chatwootService.SendMessage(msgChoise)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// loop through the input items and send them to ChatWoot
		for k, v := range typebotResponse.Input.Items {
			// Convert k to string
			key := strconv.Itoa(k + 1)
			// Send to ChatWoot
			_, err = chatwootService.SendMessage(fmt.Sprintf("[%s] - %s", key, v.Content))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			time.Sleep(1 * time.Second)
		}
	}

	w.Write([]byte("OK"))
}
